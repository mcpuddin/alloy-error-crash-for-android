var options = ['a','b','c','d','e'];

var optionsDialogOpts = {
  options:options,
  cancel:options.length-1,
  selectedIndex: 0
};
var dialog1 = Titanium.UI.createOptionDialog(optionsDialogOpts);
var dialog2 = Titanium.UI.createOptionDialog(optionsDialogOpts);
var dialog3 = Titanium.UI.createOptionDialog(optionsDialogOpts);
var dialog4 = Titanium.UI.createOptionDialog(optionsDialogOpts);
var dialog5 = Titanium.UI.createOptionDialog(optionsDialogOpts);

function showDialog1() { dialog1.show(); }
function showDialog2() { dialog2.show(); }
function showDialog3() { dialog3.show(); }
function showDialog4() { dialog4.show(); }
function showDialog5() { dialog5.show(); }