function Controller() {
    function clickLogin() {
        $.win_login.close();
        Alloy.createController("main").getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "login";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win_login = Ti.UI.createWindow({
        backgroundColor: "#fff",
        id: "win_login"
    });
    $.__views.win_login && $.addTopLevelView($.__views.win_login);
    $.__views.__alloyId1 = Ti.UI.createView({
        id: "__alloyId1"
    });
    $.__views.win_login.add($.__views.__alloyId1);
    $.__views.username = Ti.UI.createTextField({
        width: "200dp",
        autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        id: "username"
    });
    $.__views.__alloyId1.add($.__views.username);
    $.__views.password = Ti.UI.createTextField({
        width: "200dp",
        autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        id: "password"
    });
    $.__views.__alloyId1.add($.__views.password);
    $.__views.loginButton = Ti.UI.createButton({
        title: "Login",
        id: "loginButton"
    });
    $.__views.__alloyId1.add($.__views.loginButton);
    clickLogin ? $.__views.loginButton.addEventListener("click", clickLogin) : __defers["$.__views.loginButton!click!clickLogin"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.loginButton!click!clickLogin"] && $.__views.loginButton.addEventListener("click", clickLogin);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;