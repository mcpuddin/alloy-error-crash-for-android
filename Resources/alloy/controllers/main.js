function Controller() {
    function showDialog1() {
        dialog1.show();
    }
    function showDialog2() {
        dialog2.show();
    }
    function showDialog3() {
        dialog3.show();
    }
    function showDialog4() {
        dialog4.show();
    }
    function showDialog5() {
        dialog5.show();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "main";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win_main = Ti.UI.createWindow({
        backgroundColor: "#fff",
        id: "win_main"
    });
    $.__views.win_main && $.addTopLevelView($.__views.win_main);
    $.__views.__alloyId2 = Ti.UI.createView({
        id: "__alloyId2"
    });
    $.__views.win_main.add($.__views.__alloyId2);
    $.__views.text1 = Ti.UI.createTextField({
        width: "200dp",
        autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        top: "40",
        id: "text1",
        editable: "false"
    });
    $.__views.__alloyId2.add($.__views.text1);
    showDialog1 ? $.__views.text1.addEventListener("click", showDialog1) : __defers["$.__views.text1!click!showDialog1"] = true;
    $.__views.text2 = Ti.UI.createTextField({
        width: "200dp",
        autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        top: "80",
        id: "text2",
        editable: "false"
    });
    $.__views.__alloyId2.add($.__views.text2);
    showDialog2 ? $.__views.text2.addEventListener("click", showDialog2) : __defers["$.__views.text2!click!showDialog2"] = true;
    $.__views.text3 = Ti.UI.createTextField({
        width: "200dp",
        autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        top: "120",
        id: "text3",
        editable: "false"
    });
    $.__views.__alloyId2.add($.__views.text3);
    showDialog3 ? $.__views.text3.addEventListener("click", showDialog3) : __defers["$.__views.text3!click!showDialog3"] = true;
    $.__views.text4 = Ti.UI.createTextField({
        width: "200dp",
        autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        top: "160",
        id: "text4",
        editable: "false"
    });
    $.__views.__alloyId2.add($.__views.text4);
    showDialog4 ? $.__views.text4.addEventListener("click", showDialog4) : __defers["$.__views.text4!click!showDialog4"] = true;
    $.__views.text5 = Ti.UI.createTextField({
        width: "200dp",
        autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        top: "200",
        id: "text5",
        editable: "false"
    });
    $.__views.__alloyId2.add($.__views.text5);
    showDialog5 ? $.__views.text5.addEventListener("click", showDialog5) : __defers["$.__views.text5!click!showDialog5"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var options = [ "a", "b", "c", "d", "e" ];
    var optionsDialogOpts = {
        options: options,
        cancel: options.length - 1,
        selectedIndex: 0
    };
    var dialog1 = Titanium.UI.createOptionDialog(optionsDialogOpts);
    var dialog2 = Titanium.UI.createOptionDialog(optionsDialogOpts);
    var dialog3 = Titanium.UI.createOptionDialog(optionsDialogOpts);
    var dialog4 = Titanium.UI.createOptionDialog(optionsDialogOpts);
    var dialog5 = Titanium.UI.createOptionDialog(optionsDialogOpts);
    __defers["$.__views.text1!click!showDialog1"] && $.__views.text1.addEventListener("click", showDialog1);
    __defers["$.__views.text2!click!showDialog2"] && $.__views.text2.addEventListener("click", showDialog2);
    __defers["$.__views.text3!click!showDialog3"] && $.__views.text3.addEventListener("click", showDialog3);
    __defers["$.__views.text4!click!showDialog4"] && $.__views.text4.addEventListener("click", showDialog4);
    __defers["$.__views.text5!click!showDialog5"] && $.__views.text5.addEventListener("click", showDialog5);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;